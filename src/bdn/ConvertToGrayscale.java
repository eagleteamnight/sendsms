/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bdn;

/**
 *
 * @author jcrbsa
 */
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
 
import javax.imageio.ImageIO;
 
 
public class ConvertToGrayscale {
 
 
	public static void convertTograyscale(String filename){
 
		BufferedImage img = null;
 
		try {
			img = ImageIO.read(new File(filename));
		} catch (IOException e) {
			System.out.println("Failed to read image!");
		}
 
 
		if(img == null){
			System.out.println("Failed to decode image!");
			System.exit(-1);
		}
 
		BufferedImage imgGray = new BufferedImage(img.getWidth(),img.getHeight(),BufferedImage.TYPE_INT_RGB);
 
		/*Iterating over every pixel of the color image */
		int rgbVal;	
		int r,g,b;	
		int lum;
		int grayVal;	
		for(int i=0;i<img.getWidth();i++){
			for(int j=0;j<img.getHeight();j++){
 
				rgbVal = img.getRGB(i,j);
 
				/* RGB --> 32 Bit Integer		
				 * 1 Byte (alpha) | 1 Byte (red) | 1 Byte (green) | 1 Byte (blue) 
				 */
				r = (rgbVal>>16) & 0xff; //red
				g = (rgbVal>>8) & 0xff; //green
				b = rgbVal & 0xff; //blue
 
				// Convert 
                                
                                
                              
                                if(r<=255 && r >=253){
                                    r = 0;
                                }else{
                                    r= 255;
                                }
                                
                                
                                if(g <= 121 && g >= 119){
                                    g = 0;
                                }
                                else{
                                    g = 255;
                               }
                                
                                if(b > 0){
                                    b = 255;
                                }
                              
                                
                                
                               if((r >0 | g >0 | b > 0) ){
                                   r=255;
                                   g=255;
                                   b=255;
                               }
                                 
                                    
				//lum = (int) Math.round(r + g + b);			
				grayVal = (r << 16) | (g << 8) | b;
                                            
				imgGray.setRGB(i,j,grayVal);
 
			}
		}

 
		try {
			File f = new File("E:\\TESSERACT-OCR\\captcha.jpg");
			ImageIO.write(imgGray, "jpg", f);
		} catch (IOException e) {
			System.out.println("Failed to write gray image!");
		}
 
 
	}
 
 /*
	public static void main(String[] args) {
 
		String filename;	
 
		try{
			filename = "C:\\Documents and Settings\\jcrbsa\\Desktop\\download.png";		
			convertTograyscale(filename);	
 
		}catch(IndexOutOfBoundsException e){
			System.out.println("You have to specifiy the filepath as a command line argument!");
		}
	}
 */
}
