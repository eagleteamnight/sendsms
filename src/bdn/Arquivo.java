/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bdn;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;

/**
 *
 * @author jcrbsa
 */
public class Arquivo {
    
  /*  public static void main(String[] args) throws IOException {
        // replace this with a known encoding if possible
  
            handleFile("E:\\TESSERACT-OCR\\test\\out.txt");
            deleteFile("E:\\TESSERACT-OCR\\test\\", ".txt");
        
    }*/

    public static String handleFile(String filename)
            throws IOException {
        try (InputStream in = new FileInputStream(new File(filename));
             Reader reader = new InputStreamReader(in, Charset.defaultCharset());
             // buffer for efficiency
             Reader buffer = new BufferedReader(reader)) {
           return handleCharacters(buffer);
        }
    }

    private static String handleCharacters(Reader reader)
            throws IOException {
        String retorno = " ";
        
        int cont = 0;
        int r;
        while ((r = reader.read()) != -1) {
            char ch = (char) r;
            if(Character.isLetter(ch) || Character.isDigit(ch)){
            System.out.println("Do something with " + ch);
            if (cont==0){
            retorno = String.valueOf(ch);
            }else{
                retorno += String.valueOf(ch);
            }
            }
            cont++;
        }
        return retorno;
        //System.out.println(retorno);
        
    }
    
    public boolean transferFile(String in , String out){
           
        File entrada = new File(in);
        
        return entrada.renameTo(new File(out));
    }
    

    
    public static void deleteFile(String parentDirectory, String deleteExtension){
	        File parentDir = new File(parentDirectory);
	        // Put the names of all files ending with .txt in a String array
	        String[] listOfTextFiles = parentDir.list();
	 
	        if (listOfTextFiles.length == 0) {
	            System.out.println("There are no text files in this direcotry!");
	            return;
	        }
	 
	        File fileToDelete;
	 
	        for (String file : listOfTextFiles) {
	 
	            //construct the absolute file paths...
	            String absoluteFilePath = new StringBuffer(parentDirectory).append(File.separator).append(file).toString();
	 
	            //open the files using the absolute file path, and then delete them...
                    if(file.endsWith(deleteExtension)){
	            fileToDelete = new File(absoluteFilePath);
	            boolean isdeleted = fileToDelete.delete();
	            System.out.println("File : " + absoluteFilePath + " was deleted : " + isdeleted);
                    }
	        }
	    }
        
    }
    

