/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bdn;

/**
 *
 * @author jcrbsa
 */
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.File;
import javax.imageio.ImageIO;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;
public class Main {
   public static void main( String[] args )
   {
   try {
      //System.loadloadLibrary("C:\\Documents and Settings\\jcrbsa\\Desktop\\opencv-2.4.1.jar");
      File input = new File("C:\\Documents and Settings\\jcrbsa\\Desktop\\captcha.png");
      BufferedImage image = ImageIO.read(input);	

      
      byte[] data = ((DataBufferByte) image.getRaster().
      getDataBuffer()).getData();
      Mat mat = new Mat(image.getHeight(), image.getWidth(), CvType.CV_8UC3);
      mat.put(0, 0, data);

      Mat mat1 = new Mat(image.getHeight(),image.getWidth(),CvType.CV_8UC1);
      Imgproc.cvtColor(mat, mat1, Imgproc.COLOR_RGB2GRAY);

      byte[] data1 = new byte[mat1.rows()*mat1.cols()*(int)(mat1.elemSize())];
      mat1.get(0, 0, data1);
      BufferedImage image1=new BufferedImage(mat1.cols(),mat1.rows()
      ,BufferedImage.TYPE_BYTE_GRAY);
      image1.getRaster().setDataElements(0,0,mat1.cols(),mat1.rows(),data1);

      File ouptut = new File("C:\\Documents and Settings\\jcrbsa\\Desktop\\grayscale.jpg");
      ImageIO.write(image1, "jpg", ouptut);
      } catch (Exception e) {
         System.out.println("Error: " + e.getMessage());
      }
   }
   
   void convertToGrayscale(final BufferedImage image){
    for(int i=0; i<image.getWidth(); i++){
        for(int j=0; j<image.getHeight(); j++){
            int color = image.getRGB(i,j);
 
            int alpha = (color >> 24) & 255;
            int red = (color >> 16) & 255;
            int green = (color >> 8) & 255;
            int blue = (color) & 255;
 
            final int lum = (int)(0.2126 * red + 0.7152 * green + 0.0722 * blue);
 
            alpha = (alpha << 24);
            red = (lum << 16);
            green = (lum << 8);
            blue = lum;
 
            color = alpha + red + green + blue;
 
            image.setRGB(i,j,color);
        }
    }
}
}