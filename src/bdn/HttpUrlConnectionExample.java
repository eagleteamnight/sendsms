/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bdn;

/**
 *
 * @author jcrbsa
 */
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.net.ssl.HttpsURLConnection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class HttpUrlConnectionExample {

    private List<String> cookies;
    private HttpURLConnection conn;
    private HttpURLConnection conn2;
    private final String USER_AGENT = "Mozilla/5.0";
    public static String logout = "https://minha.oi.com.br/portal/site/MinhaOi/realiza_logout";
    public static String login = "https://minha.oi.com.br/portal/site/MinhaOi/ProcessarLogin";
    public static String hostTorpedoOi = "torpedo.oiloja.com.br";
    public static String hostMinhaOi = "minha.oi.com.br";

    public static void main(String[] args) throws Exception {

        String url = "https://login.oi.com.br/nidp/idff/sso?id=sso&sid=0&option=credential&sid=0";
        //String gmail = "https://mail.google.com/mail/";

        HttpUrlConnectionExample http = new HttpUrlConnectionExample();

        // make sure cookies is turn on
        CookieHandler.setDefault(new CookieManager());

        // 1. Send a "GET" request, so that you can extract the form's data.
       // String page = http.GetPageContent(url);

       // String postParams = http.getFormParams(page, "jcrbsa", "r7b2s7a2");

        // 2. Construct above post's content and then send a POST request for
        // authentication
        //System.out.println("Autenticando...");
        while(true){
        System.out.println("------------------- Request Form ------------------");
        String page = http.sendPost("http://torpedo.oiloja.com.br/oitorpedo/EnviaSMSController",http.Paramas());
         System.out.println("------------------- Respost Server ------------------");
        System.out.println(page);
        System.out.println("-------------------GET CAPTCHA ------------------");
        http.GetCaptcha("http://torpedo.oiloja.com.br/oitorpedo/captcha.jpg");
        System.out.println("-------------------Convert Gray Scale ------------------");
        new ConvertToGrayscale().convertTograyscale("C:\\download.png");
        System.out.println("------------------- Solve Captcha ------------------");
        new Captcha().solveCaptcha();
        String captcha = new Arquivo().handleFile("E:\\TESSERACT-OCR\\out.txt");
        System.out.println("Captcha value = " + captcha);
        //new Arquivo().transferFile("", "");
        System.out.println("------------------- Deleted Files ------------------");
        new Arquivo().deleteFile("E:\\TESSERACT-OCR\\", ".txt");
        new Arquivo().deleteFile("E:\\TESSERACT-OCR\\", ".jpg");
                System.out.println("------------------- Attributed Parameters ------------------");
       String postParams = http.getFormParams(page,"bruno", "81", "85121548", "oi", captcha);
       System.out.println("------------------- Send Form ------------------");
       page = http.sendPost2("http://torpedo.oiloja.com.br/oitorpedo/EnviaSMSController",postParams);
       System.out.println("------------------- Respost Server ------------------");
       System.out.println(page);
       }
       
      /*
        System.out.println("-----------------GET page-----------------");
        String result = http.GetPageContentHTTP("http://www.oi.com.br/ArquivosEstaticos/oi/index_torpedo.html");
        System.out.println(result);
*/
       //page = http.GetPageContent(logout);


    }

    private String sendPost(String url, String postParams) throws Exception {

        URL obj = new URL(url);
        conn = (HttpURLConnection) obj.openConnection();

        // Acts like a browser
        conn.setUseCaches(false);
        conn.setRequestMethod("POST");
        
	conn.setRequestProperty("Host", "torpedo.oiloja.com.br");
	conn.setRequestProperty("User-Agent", USER_AGENT);
	conn.setRequestProperty("Accept",
		"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
	conn.setRequestProperty("Accept-Language", "en-US,en;q=0.5");	
        conn.setRequestProperty("keep-alive", "300");
	conn.setRequestProperty("Connection", "keep-alive");
        conn.setRequestProperty("Referer", "http://www.oi.com.br/ArquivosEstaticos/oi/index_torpedo.html");
	conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
	conn.setRequestProperty("Content-Length", Integer.toString(url.length() + Paramas().length()));



        conn.setDoOutput(true);
        conn.setDoInput(true);

        // Send post request
        System.out.println("Enviando requisicao...");;
        DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
        wr.writeBytes(postParams);
        wr.flush();
        wr.close();


        int responseCode = conn.getResponseCode();
        System.out.println("\nSending 'POST' request to URL : " + url);
        System.out.println("Post parameters : " + postParams);
        System.out.println("Response Code : " + responseCode);

        System.out.println("Recebendo resposta...");
        BufferedReader in =
                new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine + "\n");
        }
       
        in.close();

        return response.toString();
    }
    
    
    private String sendPost2(String url, String postParams) throws Exception {

        URL obj = new URL(url);
        conn = (HttpURLConnection) obj.openConnection();

        // Acts like a browser
        conn.setUseCaches(false);
        conn.setRequestMethod("POST");
        
	conn.setRequestProperty("Host", "torpedo.oiloja.com.br");
	conn.setRequestProperty("User-Agent", USER_AGENT);
	conn.setRequestProperty("Accept",
		"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
	conn.setRequestProperty("Accept-Language", "en-US,en;q=0.5");	
     
        conn.setRequestProperty("keep-alive", "300");
	conn.setRequestProperty("Connection", "keep-alive");
        conn.setRequestProperty("Referer", "http://torpedo.oiloja.com.br/oitorpedo/EnviaSMSController");
	conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
	conn.setRequestProperty("Content-Length", Integer.toString(url.length() + Paramas().length()));



        conn.setDoOutput(true);
        conn.setDoInput(true);

        // Send post request
        System.out.println("Enviando requisicao...");;
        DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
        wr.writeBytes(postParams);
        wr.flush();
        wr.close();


        int responseCode = conn.getResponseCode();
        System.out.println("\nSending 'POST' request to URL : " + url);
        System.out.println("Post parameters : " + postParams);
        System.out.println("Response Code : " + responseCode);

        System.out.println("Recebendo resposta...");
        BufferedReader in =
                new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine + "\n");
        }
       
        in.close();
        
        setCookies(conn.getHeaderFields().get("Set-Cookie"));

        return response.toString();
    }
    private void GetCaptcha(String url) throws Exception {

        URL obj = new URL(url);
        conn2 =  (HttpURLConnection) obj.openConnection();
        
        // default is GET
        conn2.setRequestMethod("GET");
        conn2.setRequestProperty("Host", "torpedo.oiloja.com.br");
        conn2.setRequestProperty("Referer","http://torpedo.oiloja.com.br/oitorpedo/EnviaSMSController");

        conn2.setUseCaches(false);
      
        // act like a browser
        conn2.setRequestProperty("User-Agent", USER_AGENT);
        conn2.setRequestProperty("Accept","image/png,image/*;q=0.8,*/*;q=0.5");
        conn2.setRequestProperty("Accept-Language", "pt-br,pt;q=0.8,en;q=0.7,en-us;q=0.5,fr;q=0.3,es;q=0.2");
        
	//conn.setRequestProperty("Connection", "keep-alive");
        /*
               if (cookies != null) {
		for (String cookie : this.cookies) {
			conn.addRequestProperty("Cookie", cookie.split(";", 1)[0]);
		}
	}
  
       */
        
        int responseCode = conn2.getResponseCode();
        System.out.println("\nSending 'GET' request to URL : " + url);
         System.out.println("Response Code : " + responseCode);
         
         
         InputStream is = obj.openStream();
       try{  
          System.out.print("Copying resource (type: " +
                           conn.getContentType());
          Date date=new Date(conn.getLastModified());
          System.out.println(", modified on: " +
             date.toLocaleString() + ")...");
          System.out.flush();
          FileOutputStream fos=null;
          /*if (args.length < 2)
          {
              String localFile = "C:\\Documents and Settings\\jcrbsa\\Desktop\\download.png";
              // Get only file name
              StringTokenizer st=new StringTokenizer(url.getFile(), "/");
              while (st.hasMoreTokens())
                     localFile=st.nextToken();
              fos = new FileOutputStream(localFile);
          }
          else*/
              fos = new FileOutputStream("C:\\download.png");
          int oneChar, count=0;
          while ((oneChar=is.read()) != -1)
          {
             fos.write(oneChar);
             count++;
          }
          is.close();
          fos.close();
          System.out.println(count + " byte(s) copied");
      }
      catch (MalformedURLException e)
      { System.err.println(e.toString()); }
      catch (IOException e)
      { System.err.println(e.toString()); }
  
         
        
   
        setCookies(conn2.getHeaderFields().get("Set-Cookie"));

        //return response.toString();

    }
    

    private String GetPageContent(String url) throws Exception {

        URL obj = new URL(url);
        conn = (HttpsURLConnection) obj.openConnection();

        // default is GET
        conn.setRequestMethod("GET");

        conn.setUseCaches(false);

        // act like a browser
        conn.setRequestProperty("User-Agent", USER_AGENT);
        conn.setRequestProperty("Accept",
                "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
        conn.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
        if (cookies != null) {
		for (String cookie : this.cookies) {
			conn.addRequestProperty("Cookie", cookie.split(";", 1)[0]);
		}
	}
        int responseCode = conn.getResponseCode();
         System.out.println("\nSending 'GET' request to URL : " + url);
         System.out.println("Response Code : " + responseCode);
         
        BufferedReader in =
                new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine+ "\n");
        }
        in.close();

        // Get the response cookies
        setCookies(conn.getHeaderFields().get("Set-Cookie"));

        return response.toString();

    }

   
    public String Paramas() throws UnsupportedEncodingException{
        
        List<String> paramList = new ArrayList<String>();
          paramList.add("mostraPropaganda=" + URLEncoder.encode("N", "UTF-8"));
            paramList.add("hidParametro=" + URLEncoder.encode("LIBERADO", "UTF-8"));
        StringBuilder result = new StringBuilder();
        for (String param : paramList) {
            if (result.length() == 0) {
                result.append(param);
            } else {
                result.append("&" + param);
            }
         }
        return result.toString();
    }
    public String getFormParams(String html, String user, String ddd,String phone, String message, String captcha)
            throws UnsupportedEncodingException {

        System.out.println("Extracting form's data...");

        Document doc = Jsoup.parse(html);

        // Google form id
        //Elements formElements = doc.getElementsByTag("form");
        //Element loginform  = formElements.get(0);

        List<String> paramList = new ArrayList<String>();


                paramList.add(doc.getElementsByTag("input").get(1).attr("name") + "=" + URLEncoder.encode(user, "UTF-8"));
                paramList.add(doc.getElementsByTag("input").get(2).attr("name") + "=" + URLEncoder.encode(ddd, "UTF-8"));
                paramList.add(doc.getElementsByTag("input").get(3).attr("name") + "=" + URLEncoder.encode(phone, "UTF-8"));
                paramList.add(doc.getElementsByTag("input").get(4).attr("name") + "=" + URLEncoder.encode(captcha, "UTF-8"));
                paramList.add(doc.getElementsByTag("textarea").get(0).attr("name") + "=" + URLEncoder.encode(message, "UTF-8"));
           


        // build parameters list
        StringBuilder result = new StringBuilder();
        for (String param : paramList) {
        {
             if (result.length() == 0) {
                result.append(param);
            } else {
                result.append("&" + param);
            }
            }
         }
        return result.toString();
    }

    public List<String> getCookies() {
        return cookies;
    }

    public void setCookies(List<String> cookies) {
        this.cookies = cookies;
    }

    private void sendPost(String httptorpedooilojacombroitorpedoEnviaSMSCo) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}